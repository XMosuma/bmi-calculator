# BMI Calculator

Use BMI (Body Mass Index) calculator to calculate your body fat estimate. BMI is a useful tool to gauge your risk against various diseases and is often used by life insurers to assess health risks when issuing life cover policies.
