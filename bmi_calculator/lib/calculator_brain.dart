import 'dart:math';
class CalculatorBrain{
  CalculatorBrain({required this.height,required this.weight});
  final int height;
  final int weight;
  late double _bmi;

  String calculateBMI(){
    _bmi = weight/pow(height/100,2);
    return _bmi.toStringAsFixed(1);
  }
  String getResult(){
    if(_bmi >=25){
      return 'Overweight';
    }
    else if(_bmi > 18.5 && _bmi <25){
      return 'Normal';
    }
    else{
      return 'Underweight';
    }
  }
  String getInterpretation(){
    if(_bmi >=25){
      return 'Your BMI  is higher than a normal BMI. Try to exercise more';
    }
    else if(_bmi > 18.5 && _bmi <25){
      return 'You have a normal BMI. Keep it that way';
    }
    else{
      return 'Your BMI is very Low. You can increase it by eating health';
    }
  }
}